package com.example.test.controller;

import com.example.test.model.HelloBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String hello() {
        return "hi";
    }

    @GetMapping("hello-world-bean")
    public HelloBean bean() {
        return new HelloBean("hello");
    }
}
